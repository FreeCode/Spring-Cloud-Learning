package com.lpf.eureka.client.indicator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class EurekaClientHealthIndicator implements HealthIndicator {

    //日志记录器
    Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Health health() {
        logger.info("Eureka Server 对 Eureka Client 进行健康检查：实例棒棒哒");
        return new Health.Builder().withDetail("usercount", 10) //自定义监控内容
                .withDetail("userstatus", "up").up().build();
    }
}