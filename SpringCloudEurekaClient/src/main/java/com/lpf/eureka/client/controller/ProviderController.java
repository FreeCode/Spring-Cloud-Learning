package com.lpf.eureka.client.controller;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 直接访问地址：http://127.0.0.1:7005/dept/simlpe/20
 */

@RestController
public class ProviderController {

    //日志记录器
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    DiscoveryClient discoveryClient;

    @GetMapping("/dept/simlpe/{id}")
    public String simlpe(@PathVariable("id") long id) {
        // 获取已注册到Eureka的实例信息
        String result = JSON.toJSONString(discoveryClient.getServices());
        logger.info(result);
        return "来自服务提供者：" + result;
    }

}